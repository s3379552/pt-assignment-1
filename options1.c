/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #1 
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "assign1.h"
#include "math.h"
#include <string.h>
/***************************************************************************
* This source file contains important functions to be developed and
* used by various menu options, as indicated. Note that every
* function has as its first parameter the optionsStats array, which
* will be appropriately updated for later reporting of menu option 6.
* You may ignore this parameter and its relevance to each function
* until you develop the sessionSummary() function.
 **************************************************************************/

/**************************************************************************
* perfect_squares() - implements the "Perfect Squares" option (requirement 
* 2) from the main menu. You need to determine whether the number passed
* in is a perfect square and let the user know if it is or not. You also
* need to print the perfect square before and after this value. Finally
* you need to update the option_stats variable appropriately.
**************************************************************************/
void perfect_squares(int * option_stats, unsigned number)
{
	double divisor = 1.0000000;
	double result;
	int previousNum, nextNum;
	int prevCheck = FALSE;
	int nextCheck = FALSE;
   /*Take the square root of the passed number.*/
	result = sqrt(number);
   /* If the remainder of the square root operation is 0, the passed input
 *    is a perfect square. Otherwise, the number is not a perfect square.
 *    The result is divided by 1 to use this fuction and not affect 
 *    the value.*/
	if(fmod(result, divisor) == 0)
	{
		printf("%d is a perfect square.\n", number);
	}
	else
	{
		printf("%d is not a perfect square.\n", number);
	}
	previousNum = number;
	nextNum = number;
   /* Loop for finding previous perfect square. Start the previous number
 *    counter at the input value. Increment it down one. If the new
 *    previous number is zero, there is no pervious perfect square.
 *    If the square root of the new previous number has no remainder, then
 *    this previous number is the previous perfect square. Once this is
 *    found, break out of this loop.*/
	do
	{
		--previousNum;
		result = sqrt(previousNum);
		if (previousNum == 0)
		{
			printf("There is no perfect square before %d.\n", number);
			break;
		}
		if(fmod(result, divisor) == 0)
		{
			printf("Perfect square before: %d\n", previousNum);
			prevCheck = TRUE;	
		}

	} while(prevCheck ==  FALSE);
   /* Loop for finding next perfect square. Operates very similarly to the
 *    above loop for finding previous perfect square, but there is no limit
 *    to which the next number can be.*/
	do
	{
		++nextNum;
		result = sqrt(nextNum);
		if(fmod(result, divisor) == 0)
		{
			printf("Perfect square after: %d\n", nextNum);
			nextCheck = TRUE;
		}
	} while (nextCheck == FALSE);
   *option_stats = *option_stats + 1;
}

/**************************************************************************
* ascii_to_binary() - implements the "ASCII to Binary Generator" option
* (requirement 3) from the main menu. Essentially, you need to implement
* an algorithm for conversion of integers to binary and apply this to 
* every character in the input array (called 'ascii' here) and print out
* the result. Finally, you need to update the option_stats array 
* appropriately 
**************************************************************************/
void ascii_to_binary(int * option_stats, char * ascii)
{
	int charInts[OPTION_2_LENGTH];
	int currentChar, i;
	int digit_count=6;
	char binary_string[BINARY_ARRAY_LENGTH+EXTRA_SPACES];
   int offset;

   /* Loop that increments for each character passed to the function. If
 *    the current character is EOF, break out of the loop. Otherwise, 
 *    place the integer value of the character in another array, and copy
 *    this value into another integer variable for conversion to binary.*/
	for(i = 0; i < OPTION_2_LENGTH; i++) 
	{
      if(ascii[i] == '\0')
		{
			break;
		}
		charInts[i] = ascii[i];
		currentChar = charInts[i];
      /* Used for counting backwards through the output array when doing 
 *       the binary conversion. This allows the output to go from Least 
 *       Signifigant Bit to most signifigant bit for each character, 
 *       as this is the order done by the conversion.*/
      offset = 0;
      /* The loop for converting a character to its binary value. It takes
 *       the current integer value (starting from the integer value of the
 *       original character), and does modular divison by 2 of this value.
 *       if the mod operation equals 0 (no remainder), then input a zero
 *       in the current position of the output string. If the operation
 *       equals 1 (remainder of 1), then input a 1 in the current position
 *       of the output string. Divide this integer value by two to get the
 *       next integer value to mod operate on for the next binary character.
 *       Also, increment the offset value down one, as you're inserting the
 *       binary values backwards from LSB to MSB. Once the integer has 
 *       reached zero, the binary string of the original value has been 
 *       created.*/
		do
		{
			if(currentChar%2 == 0)
         {
            binary_string[digit_count+offset] = '0';
         }
         else
         {
            binary_string[digit_count+offset] = '1';
         }
         currentChar = currentChar/2;
         offset--;
		} while (currentChar > 0);
      /* Once a binary string for a character has been created, increment
 *       the counter for current position in the output string by one, 
 *       insert a space (to seperate the different binary values), then 
 *       increment the count by 7 or 6(as you need to start inserting 
 *       backwards from the LSB of the next character, and all ASCII 
 *       characters have 8 bit binary values.*/
      digit_count++;
		binary_string[digit_count] = ' ';
      /*if the next character in the input string cannot be represented
 *       by 6 binary digits (greater than 63 ASCII value), reserve 7 
 *       digits for it in the digit_count array. Otherwise, only reserve
 *       6 digits */
      if(ascii[i+1] > 63 && ascii[i+1] != '\n')
         digit_count = digit_count + 7;
      else if(ascii[i+1] != '\n')
         digit_count = digit_count + 6;
      else
         break;
	}
   /* Once all characters have been converted and their binary values have
 *    been put in the output string, add an EOF to the end of the string
 *    with the EOF position accounting for the extra 7 spaces added to
 *    count after converting the last character. Finally, print the 
 *    string */
   binary_string[digit_count - 6] = '\0';
   printf("Binary Representation: %s\n", binary_string);
   *option_stats = *option_stats + 1;
}

/**************************************************************************
* matching_brackets() - implements the "Matching Brackets" option 
* (requirement 4) from the main menu. You need to implement an algorithm 
* which will parse a string passed in and determine whether the brackets 
* all match, including the type of brackets. You need to account for 
* {} [] () and you also need to allow for the nesting of brackets. 
* You need to tell the user whether the brackets match or not. Finally you
* need to update the option_stats array appropriately.
**************************************************************************/
void matching_brackets(int * option_stats, char * test_string)
{
   /* Create a stack, then use the stack for comparisons.
 *    Read each input open bracket into the stack (push)
 *    if the input character is a closed bracket, compare it to the open 
 *    bracket on top of the stack. If its the opposing bracket, its valid 
 *    and push the related open bracket off the stack. Otherwise, deny 
 *    string.*/
   int i;
   char stack[OPTION_3_LENGTH];
   int top_of_stack = 0;
   for(i = 0; i < OPTION_3_LENGTH; i++)
   {
      /* If the current character is an open bracket, add it to the top of
 *       the stack and increment the top of stack pointer.*/
      if(test_string[i] == '(' || test_string[i] == '{' || 
         test_string[i] == '[')
      {
         stack[top_of_stack] = test_string[i];
         ++top_of_stack;
      }
      else if(test_string[i] == '}' || test_string[i] == ']')
      {
         /* the ASCII value for } and ] are 2 larger than their respective 
 *          open bracket values, so adding 2 to the values of the current 
 *          top of stack value allows a comparison for both of these 
 *          brackets in one line.*/
         if(test_string[i] != (stack[top_of_stack-1]+2))
         {
            /* if brackets do not match, end function */
            printf("Brackets do not match.\n");
            return;
         }
         else
         {
            /* otherwise, pop current character off the stack and move
 *             top of stack back down one. Replace the matched open
 *             bracket with a null character */
            --top_of_stack;
            stack[top_of_stack] = 0;
         }   
      }
      else if(test_string[i] == ')')
      {
         /* Unlike the {} and [] brackets, the closed bracket value of () 
 *          is only one more than its open bracket, so it needs its own 
 *          comparison.*/
         if(test_string[i] != (stack[top_of_stack-1] + 1))
         {
            printf("Brackets do not match.\n");
            return;
         }
         else
         {
            --top_of_stack;
            stack[top_of_stack] = 0;
         }
      }
   }
   /* If the whole stack it not empty, or the top of stack pointer is not
 *    pointing to the first value in the stack, the string is denied.*/ 
   if(stack[0] != 0 || top_of_stack != 0)
   {
      printf("Brackets do not match.\n");
      return;
   }
   printf("Brackets match.\n");
   *option_stats = *option_stats + 1;
}

/**************************************************************************
* format_text() - implements the "Formatting Text" option (requirement 6)
* from the main menu. You will need to parse the text, adding newlines 
* so that no line is longer than "width" which means you will need to 
* replace the first white space character before this with the newline. You
* then need to insert extra spaces so that lines are spaced as evenly as 
* possible. Finally you want to update the option_stats array appropriately.
* 
**************************************************************************/
void format_text(int * option_stats, unsigned width, char * text)
{ 
   const char delim[2] = " ";
   char *currentWord;
   /* currentLine counts how many characters have been added to the current
    line, rest are counters for space padding detection*/
   int currentLine=0, i=0, totalSpaces=0, singleWordSpaces;
   int charCount=0, charSinceSpace = 0;
   /*loop for counting first line's space padding*/
   while(1)
   {
      ++charCount;
      /* if the current character is a space, increment the space 
 *       counter for this line, and reset the counter since the last 
 *       space*/
      if(text[i] == 32)
      {
         ++totalSpaces;
         charSinceSpace = 0;
      }
      /* otherwise, increment the counter since the last space*/
      else
      {
         ++charSinceSpace;
      }
    /* if the current character count for the line is greater than 
 *    the width specified calculate the number of extra spaces to 
 *    pad, set the current text array counter back to the start 
 *    of the current word before a space, and reset all the counters 
 *    for this line.*/
      if((totalSpaces + charCount) > width)
      {
         /* divide characters since last space by total number of spaces
 *          Also minus the number of spaces in the line from this result
 *          as the printf statement for each word has 1 space already. */
         singleWordSpaces = charSinceSpace/totalSpaces;
         singleWordSpaces = singleWordSpaces - totalSpaces;
         /* Move the counter back to the start of the current word, as it
 *          is not printed on this line and will be the starting word of
 *          the next line. Also clear all line specific pointers*/
         i = i - (charSinceSpace);
         totalSpaces = 0;
         charSinceSpace = 0;
         charCount = 0;
         break;
      }
      ++i;
   }
   currentWord = strtok(text, delim);
   printf("\n");
   while(currentWord != NULL)
   {
      currentLine = currentLine + (int)(strlen(currentWord));
      /*Add the new string token to the current line length*/
      /*If the currentLine width (including the latest token) is larger
       than the nominated width value, calculate the new space padding, 
       add a new line, and reset currentLine back to zero. It then sets 
       the currentWord as the first word on the next line  */
      if(currentLine >= width){
         printf("\n");
         while(1)
         {
            /* If its the end of the text array, calculate the number of 
    *          spaces and break the loop*/
            if(currentWord == NULL)
            {
               printf("charSinceSpace value - %d\n", charSinceSpace);
               singleWordSpaces = (charSinceSpace/totalSpaces)-totalSpaces;
               i = i - (charSinceSpace+1);
               totalSpaces = 0;
               charSinceSpace = 0;
               charCount = 0;
               break;
            }
            ++charCount;
            /* if the current character is a space, increment the space 
    *          counter for this line, and reset the counter since the last 
    *          space*/
            if(text[i] == 32)
            {
               ++totalSpaces;
               charSinceSpace = 0;
            }
            /* otherwise, increment the counter since the last space*/
            else
            {
               ++charSinceSpace;
            }
            /* if the current character count for the line is greater than 
    *          the width specified calculate the number of extra spaces to 
    *          pad, set the current text array counter back to the start 
    *          of the current word before a space, and reset all the 
    *          counters for this line.*/ 
            if((totalSpaces + charCount) > width)
            {
               /* divide characters since last space by total number of 
    *             spaces in line. Then minus the total number of spaces 
    *             from this value, as each word in the printf statement 
    *             will already come with one space. If there are no spaces 
    *             (single word), add no spaces. */
               if(totalSpaces == 0)
                  singleWordSpaces = 0;
               else{
                  singleWordSpaces = (charSinceSpace/totalSpaces);
                  singleWordSpaces = singleWordSpaces - totalSpaces;
               }
               i = i - (charSinceSpace);
               totalSpaces = 0;
               charSinceSpace = 0;
               charCount = 0;
               break;
            }
            /* Move to next character in point array)*/
            ++i;  
         }
         /*print the first word of this new line. Add the space padding
 *          calculated from the above loop to the length of the word in
 *          the printf statement to add the extra spaces*/
         printf("%-*s ", singleWordSpaces+(int)(strlen(currentWord)), 
            currentWord);
         /* start counting the current line length again from the length of
 *          the first word*/
         currentLine =  strlen(currentWord);
         currentWord = strtok(NULL, delim);
      }
      else
      {
         /* print the next word in this line, a space to seperate from
 *          the next word, as well as any padding spaces that were 
 *          calulated from the line loop. Then, get the next word.*/
         printf("%-*s ", singleWordSpaces+(int)(strlen(currentWord)), 
            currentWord);
         currentWord = strtok(NULL, delim);
      }
   }
   printf("\n");  
   *option_stats = *option_stats + 1;
}

/**************************************************************************
* session_summary() - implements the "Session Summary" option (requirement
* 7) from the main menu. In this option you need to display the number of 
* times that each option has been run and then update the count of how 
* many times this function has been run.
**************************************************************************/
void session_summary(int * option_stats)
{
	int i;
   /* increment counter for summary selection, then reset array pointer 
 *    back to zero.*/
   option_stats = option_stats + 5;
   *option_stats = *option_stats + 1;
   option_stats = option_stats - 5;
   /*printf lines for setting up the top of the summary table */
	printf("Session Summary\n");
	printf("_______________\n");
	printf("\n");
	printf("Option Count\n");
	printf("______ _____\n");
   /* Increment the array pointer by one, as summary values start at 1 and
 *    array starts at 0*/
   option_stats = option_stats + 1;
   /* increment through the options array, printing the counter and the 
 *    current array value.*/
	for(i = 1; i < NUM_OPTION_STATS; i++)
	{
      printf("%6d %5d\n", i, *option_stats);
      option_stats = option_stats + 1; 
	}		
}
