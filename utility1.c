/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #1 
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Padgham
***************************************************************************/
#include "assign1.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**************************************************************************
* read_rest_of_line() - clear the buffer of any leftover input. Call this 
* function whenever you detect leftover input in the buffer.
a**************************************************************************/

int ctrlDFlag = FALSE;
void read_rest_of_line(void)
{
    int ch;
    /* read characters one at a time from standard input until there are
     * no characters left to read
     */
    while (ch=getc(stdin), ch!=EOF && ch!='\n');
    /* reset the error status of the input FILE pointer */
    clearerr(stdin);
}
void read_char_input(int input_size, char * outputString)
{
	/*Basis for method code take from Blackboard code example, test_fgets.c
 	test-fgets.c code created by Steven Burrows, 2004 */
	int finished = FALSE;
   int typeCheck;	
	do
	{
      /* If fgets returns null, CTRL+D has been input. Activate flag for
 *       CTRL+D input and clear values in memory allocated for 
 *       outputString */
		if(fgets(outputString, input_size + EXTRA_SPACES, stdin) == NULL)
      {
         printf("\n");
         memset(outputString, 0, input_size + EXTRA_SPACES);
         ctrlDFlag = TRUE;
      }
      typeCheck = ascii_type_check(outputString);
      if (typeCheck == FALSE)
      {
         memset(outputString, 0, input_size + EXTRA_SPACES);
         continue;
      }
      /* Check for inputs larger than the maximum allowed. If this is the
 *       case, clear buffer and ask for another input */
		if (outputString[strlen(outputString) - 1] != '\n' &&
         outputString[0] != '\0')
		{
			printf("Given input is too long, please enter a valid input.\n");
			read_rest_of_line();
		}
      /* Otherwise, input is valid. Append an EOF onto the string and exit
 *       the input loop. */
		else
		{
			outputString[strlen(outputString) - 1] = '\0';
			finished = TRUE;
		}		 	
	} while (!finished);
}

void read_int_input(int  max_int, int int_input_size, unsigned * output_int)
{
	int intFinished = FALSE;
   int typeCheck;
	char tempinput[MAX_INT_LENGTH + EXTRA_SPACES];
	do
	{
      /* Check fgets for null (CTRL+D) input, activate CTRL+D input flag
 *       and clear values in memory allocated for the fgets input. */
		if(fgets(tempinput, int_input_size + EXTRA_SPACES, stdin) == NULL)
      {
         printf("\n");
         memset(tempinput, 0, int_input_size + EXTRA_SPACES);
         ctrlDFlag = TRUE;
      }
      /* Check the users input array to make sure it is of the integer
 *       type. If it is not, clear values in memory allocated to input
 *       and restart the loop asking for input. */
      typeCheck = int_type_check(tempinput);
      if(typeCheck == FALSE)
      {
         memset(tempinput, 0, int_input_size + EXTRA_SPACES);
         continue;
      }
      /* Take the user input from fgets and convert it to integer data 
 *       types */
		*output_int = atoi(tempinput);
      /* If the integer value of the user input is greater than the
 *       maximum allowed value, clear the input buffer. Otherwise, the 
 *       input passes all checks in this function and the input loop 
 *       is quit.*/ 
		if (max_int < *output_int)
		{
			printf("Given number is too large, please enter a number in the valid range.\n");
         read_rest_of_line();
		}
		else
		{
			intFinished = TRUE;
		}
	} while (!intFinished);
}

int empty_line_check_char (char *input)
{
   /* if the first character in the input array is a newline
      or the CTRL+D flag has been activeated from an input function, 
      reset the CTRL+D flag and return TRUE. Otherwise, the input is not
      and empty line and return FALSE.*/ 
   if(input[0] == '\n' || input[0] == '\0' || ctrlDFlag == TRUE)
   {
      printf("EMPTY STRING DETECTED. Returning to menu.\n");
      ctrlDFlag = FALSE;
      return TRUE;
   }
   else
      return FALSE;
} 	
int empty_line_check_int (unsigned *input)
{
   /* operates in the same way as empty_line_check_char. Only difference is
 *    that the value of the input integer is set back to NULL as well. */
   if(*input == '\0' || *input == '\n' || ctrlDFlag == TRUE)
   {
      printf("EMPTY STRING DETECTED. Returning to menu.\n");
      *input = '0';
      ctrlDFlag = FALSE;
      return TRUE;
   }
   else
      return FALSE;
}
int int_type_check(char *input)
{
   int i;
   /* Checks that the given input string (taken directly from fgets) is of
 *    the integer data type. */
   for (i = 0; i < strlen(input); i++)
   {
      /* Checks for the end of the input string. If the end is found, all
 *       pervious characters have passed the check and return TRUE.*/
      if (input[i] == '\n' || input[i] == '\0')
         return TRUE;
      /* Check to see if the current character is a digit (0-9). If it is
 *       not, isdigit will return NULL. In this case, return FALSE.*/
      if (isdigit(input[i]) == 0)
      {
         printf("Please enter integers only.\n");
         return FALSE; 
      }
   }
   return TRUE;
}
int ascii_type_check(char * input)
{
   int i; 
   /* Checks that the given input string (taken directly from fgets) is of
 *    the ASCII data type. */

   for (i = 0; i < strlen(input); i++)
   {
      /* If the end of the string is found and it has not yet been returned,
 *       then the string is ok and should be returned.*/
      if(input[i] == '\n' || input[i] == '\0')
         return TRUE;
      /* If the entered character is not in the valid range of characters,
 *       return false*/
      if(input[i] < 32 || input[i] > 126)
      {
         printf("Please enter non-blank ASCII characters only.\n");
         return FALSE;
      }
   }
   return TRUE;
}
