/****************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #1 
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/

-----------------------------------------------------------------------------
If selected, do you grant permission for your assignment to be released as an
anonymous student sample solution?
-----------------------------------------------------------------------------

Yes

-----------------------------------------------------------------------------




Known bugs:
-----------
If entering non-integer characters into an integer input, the 
"Please enter integers only" prompt will display more than once if more
than one character is entered.
If entering non-ASCII characters, fgets will return null, sending the user
back to the main menu.


Incomplete functionality:
-------------------------
In the "Formatting Text" option, the number of padding spaces for each line
does not calculate properly. This means the line will sometimes the
specified limit.


Assumptions:
------------
In the "Formatting Text" option, the line width entered would be no more
then 50, and the length of each word would not exceed the length of a 
single line.


Any other notes for the marker:
-------------------------------
