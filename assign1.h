/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 1 2013 Assignment #1 
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Xiaodong Li
***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/* constants go here */
#define NUM_OPTION_STATS 6
#define MENU_OPTION_RANGE 6
#define OPTION_1_LENGTH 7
#define OPTION_2_LENGTH 5
#define OPTION_3_LENGTH 20
#define OPTION_4_LENGTH_TEXT 200
#define OPTION_4_LENGTH_NUM 2
#define OPTION_1_MAX 1000000
#define OPTION_4_LENGTH_MAX 50
#define BINARY_ARRAY_LENGTH 44
#define MAX_INT_LENGTH 5
/* This is used to compensate for the extra character spaces taken up by
   the '\n' and '\0' when user is asked for input through fgets(). */
#define EXTRA_SPACES 2

/* Specifies the maximum input length a user can enter for the options
   menu. */
#define MAX_OPTION_INPUT 1

/* provides us with a BOOLEAN type for using TRUE and FALSE */
typedef enum true_false
{
    FALSE=0,TRUE
} BOOLEAN;

/* function prototypes for each option to be implemented */
void perfect_squares(int * option_stats, unsigned number);
void ascii_to_binary(int * option_stats, char * ascii);
void matching_brackets(int * option_stats, char * test_string);
void format_text(int * option_stats, unsigned width, char * text);
void session_summary(int * option_stats);

/* function prototypes for additional functions contained in utility1.c */
void read_rest_of_line(void);
void read_char_input(int input_size, char * outputString);
void read_int_input(int max_int, int int_input_size, unsigned * output_int);
int empty_line_check_char(char *input);
int empty_line_check_int(unsigned *input);
int int_type_check(char * input);
int ascii_type_check(char * input);
