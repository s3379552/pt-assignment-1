/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #1 
* Full Name        : Adam Thompson
* Student Number   : s3379552
* Yallara Username : s3379552
* Course Code      : COSC1284
* Program Code     : BP094SEC8
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "assign1.h"

int main(void)
{
    /*declaration of variables*/
    int i;
    int *option_pointer;
    int option_stats[NUM_OPTION_STATS];
    unsigned selected_option;
    int inputCheck;
    int exitChose = FALSE;
    unsigned case1Input;
    char (*case2ptr)[OPTION_2_LENGTH + EXTRA_SPACES];
    char case2Input[OPTION_2_LENGTH + EXTRA_SPACES];
    char (*case3ptr)[OPTION_3_LENGTH + EXTRA_SPACES];
    char case3Input[OPTION_3_LENGTH + EXTRA_SPACES];
    char (*case4ptr)[OPTION_4_LENGTH_TEXT + EXTRA_SPACES];
    char case4Text[OPTION_4_LENGTH_TEXT + EXTRA_SPACES];
    unsigned case4Width;
   option_pointer = option_stats;
   /* initialise the option summary array with zero for every value */
   for (i = 0; i < NUM_OPTION_STATS; i++){
      option_stats[i] = 0;
   }
    /* loop for displaying the main menu and running the selected
     * option here */
   do
	{
	   printf("Main Menu\n");
	   printf("_________\n");
	   printf("1) Perfect Squares\n");
   	printf("2) ASCII to Binary Generator\n");
	   printf("3) Matching Brackets\n");
	   printf("4) Formatting Text\n");
	   printf("5) Session Summary\n");
	   printf("6) Exit\n");
   	printf("\n"); 
   	printf("Select your option:\n");
   	read_int_input(MENU_OPTION_RANGE, MAX_OPTION_INPUT, &selected_option);
	
      /*Switch statement for choosing menu option*/
	   switch(selected_option)
	   {
		   case 1 :
            /* ask for input integer from the user. Validate input, 
 *             check for empty line. Pass valid input to the perfect 
 *             squares function*/ 
			   printf("Enter a positive integer (1-1000000): ");
			   read_int_input(OPTION_1_MAX, OPTION_1_LENGTH, &case1Input);
            inputCheck = empty_line_check_int(&case1Input);
            option_pointer = &option_stats[1];
            if(inputCheck == FALSE)
            { 
               perfect_squares(option_pointer, case1Input);
            }
            break;
		   case 2 :
            /* ask for input string from the user. Validate and empty line
 *          check the input, then pass valid string to ascii-binary 
 *          function.*/
			   case2ptr = &case2Input;
			   printf("Enter a string (1-5 characters): ");
			   read_char_input(OPTION_2_LENGTH, *case2ptr);
            inputCheck = empty_line_check_char(*case2ptr);
            option_pointer = &option_stats[2];
            if(inputCheck == FALSE)
            {
               printf("OPTION VALUE - %d\n", option_stats[2]);
			      ascii_to_binary(option_pointer, *case2ptr);
            }
			   break;
		   case 3 :
            /* ask for input string from the user. Validate and empty line
 *          check the input, then pass valid string to matching brackets
 *          function*/
			   case3ptr = &case3Input;
			   printf("Enter a string (1-20 characters): ");
			   read_char_input(OPTION_3_LENGTH, *case3ptr);
            inputCheck = empty_line_check_char(*case3ptr);
            option_pointer = &option_stats[3];
            if(inputCheck == FALSE)
            {
			      matching_brackets(option_pointer, *case3ptr);
            }  
			   break;
		   case 4 :
            /* ask for input integer from the user (part 1 of user input).
 *          Validate and empty line check this input. If the input is
 *          valid, ask for input string from the user (part 2 of user 
 *          input). Validate and empty line check this input. If this
 *          input is also valid, pass both part 1 and 2 input to the
 *          fomratting text function.*/
			   case4ptr = &case4Text;
			   printf("Enter an integer (maximal number of chars ");
			   printf("per line): ");
		   	read_int_input(OPTION_4_LENGTH_MAX, OPTION_4_LENGTH_NUM, 
               &case4Width);
		   	inputCheck = empty_line_check_int(&case4Width);
            if(inputCheck == FALSE)
            {
               printf("Enter some sentences (between 150 - 200 chars: ");
			      read_char_input(OPTION_4_LENGTH_TEXT, *case4ptr);
               inputCheck = empty_line_check_char(*case4ptr);
               option_pointer = &option_stats[4];
               if(inputCheck == FALSE)
               {
			         format_text(option_pointer, case4Width, *case4ptr);
               }
               else
               {
                  break;
               }    
            }
			   break;
		   case 5 :
			   session_summary(option_stats);
			   break;
		   case 6 :
			   printf("Program now closing.\n");
			   return EXIT_SUCCESS;
		   default :
         /* If the menu input is not an integer from the 1-6 range, 
 *          ask for another input */
			   printf("Error. Incorrect Input.\n");
			   printf("Please choose another input.\n");
	   }
	} while (!exitChose);
   return EXIT_SUCCESS;
}
